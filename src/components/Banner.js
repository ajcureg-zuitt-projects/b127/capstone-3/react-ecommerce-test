//Bootstrap
import { Jumbotron, Button, Row, Col } from 'react-bootstrap';


export default function Banner(){
	return(
		<Row>
			<Col className="mt-3">
				<Jumbotron>
					<h1>Merch mo ‘to</h1>
					<p>Collect what you love.</p>
					<Button variant="primary">Order Now!</Button>
				</Jumbotron>
			</Col>
		</Row>
	)
}